Rails.application.routes.draw do
  namespace :api, default:{format: :json} do
    namespace :v1 do
      resources :users, only: [:index]

      post 'auth/login' => 'authentication#login'
      get 'users/' => 'users#index'
      get 'users/:id' => 'users#show'
      get 'notifications/' => 'notifications#index'
      get 'notifications/:id' => 'notifications#show'

      get 'user-detail/:id' => 'user_detail#show'
      post 'user-detail/' => 'user_detail#create_or_update_user_detail'

      get 'mockup-data' => 'phone_number#mockup_data'
      get 'phone-number' => 'phone_number#index'
      get 'phone-number/:id' => 'phone_number#show'
      post 'phone-number' => 'phone_number#create'
      put 'phone-number/:id' => 'phone_number#update'
    end
  end
end