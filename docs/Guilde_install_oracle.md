### Instructions for setting up the environment for Oracle database

1. **Install oracle database**

- Visit [this link](https://www.oracle.com/database/technologies/oracle19c-windows-downloads.html) to download and install oracle database version 19c. 

  *Note: Log in or create an account if you don't have one to download the installation file to your device!*

  *Images is taken from vinasupport.com*

- Extract the Oracle installation file to your desired directory and run the setup.exe file.

  ![alt text](./images/install_oracle1.png "overview")

- **Database Installation option**

  ![alt text](./images/install_oracle2.png "overview")

- **Database installation type**

  ![alt text](./images/install_oracle3.png "overview")

- **Database edition**

  ![alt text](./images/install_oracle6.png "overview")

- **Specify Oracle Home User**

  ![alt text](./images/install_oracle4.png "overview")

- **Specify the installation path of Oracle Base**

  Leave the default or you can change to another path.

  ![alt text](./images/install_oracle5.png "overview")

- **Check the installation condition**

  - Oracle will run a task to check the environment conditions for installation
  - If there are no problems during the check, you will see the “Summary” screen as shown below.
  - Click [ Install ] to proceed with the installation

  ![alt text](./images/install_oracle7.png "overview")

  - After the installation is finished, click [ Finish ] to complete

  2. **Install SQL Developer 19.4 on Windows 10**

  - Tải file: [sqldeveloper-19.4.0.354.1759-x64.zip](https://drive.google.com/file/d/130BELRMMbzJNI2uLM5Vp0EPDCJT5xTM7/view?usp=sharing) (you can download higher versions) or download link from Oracle. Note: must be logged in before downloading.

  - Extract the downloaded sqldeveloper-194.0.354.1759-x64.zip file.

    ![alt text](./images/install_sql_develop.png "overview")

  3. **Install database for the project**

- First, you create oracle database on sql developer.

- Second, you create an admin account.

  - **Step 1**: Login to the system account

  - **Step 2**: In the system account find "Orther Users", right click select "Create User". Enter username: **ADMIN** and password: **Forval1234**

    ![alt text](./images/create_admin.PNG "overview")

  - **Step 3**: In tab Granted Roles choose Default All

    ![alt text](./images/create_admin1.PNG "overview")

  - **Step 4**: In tab System Privileges check all colums Granted

    ![alt text](./images/create_admin2.PNG "overview")

  - **Step 5**: Add "alter session set "_ORACLE_SCRIPT"=true;" in tab SQL

    ![alt text](./images/create_admin3.PNG "overview")

- The last, right click on your database and select "Open SQL Worksheet"

  ![alt text](./images/setup_db.png "overview")

  

- Copy all the content in "**schema_sample_data.sql**" in the folder "**forval/Research/VueRuby/db**" into the worksheet and click the circled button.

![alt text](./images/setup_db1.png "overview")

*Note: If creating or updating a table, copy the create or update table script and put it in the above worksheet.*

