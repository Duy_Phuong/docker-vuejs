# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "tb_sct_bb_juchu_meisai", primary_key: "serial_no", id: { type: :string, limit: 4 }, force: :cascade do |t|
    t.string "kakin_tel_no", limit: 13, null: false, comment: "Billing phone number"
    t.integer "tyakusin_num", limit: 3, precision: 3, null: false
    t.string "kaisen_meigi", limit: 72, null: false
    t.string "kaisen_meigi_kana", limit: 72, null: false
    t.string "daihyo_tel_no_kubun", limit: 1, null: false, comment: "Representative phone number section"
    t.string "nob_kubun", limit: 1, null: false, comment: "NumberOnBB"
    t.string "write_104_section", limit: 1, null: false, comment: "Write 104 section"
    t.string "name_in_the_phone_book", limit: 72, null: false
    t.string "name_in_the_phone_book_kana", limit: 72, null: false
  end

  create_table "tb_sct_login_user_kanri", primary_key: "login_id", id: { type: :string, limit: 6, comment: "Login ID" }, force: :cascade do |t|
    t.string "login_user_mei", limit: 60, comment: "Login username"
    t.string "login_password", limit: 20, comment: "Login password"
    t.string "kengen_id", limit: 2, comment: "Authority ID"
    t.string "sakujo_flg", limit: 1, comment: "Delete flag"
    t.datetime "insert_dtm", precision: 6, null: false, comment: "Registered Date"
    t.string "insert_user_cd", limit: 6, null: false, comment: "Registered user"
    t.datetime "update_dtm", precision: 6, null: false, comment: "Update date and time"
    t.string "update_user_cd", limit: 6, null: false, comment: "Update user"
  end

  create_table "tb_sct_oshirase_kanri", primary_key: "seq_no", id: { type: :string, limit: 4, comment: "Sequence number" }, force: :cascade do |t|
    t.decimal "keisai_kubun", comment: "Publication category"
    t.datetime "keisai_start_dtm", precision: 6, null: false, comment: "Publication start date and time"
    t.datetime "keisai_end_dtm", precision: 6, null: false, comment: "Publication end date and time"
    t.string "keisai_msg", limit: 1024, null: false, comment: "Publication message"
    t.string "sakujo_flg", limit: 1, comment: "Delete flag"
    t.datetime "insert_dtm", precision: 6, null: false, comment: "Registered date and time"
    t.string "insert_user_cd", limit: 6, null: false, comment: "Registered user"
    t.datetime "update_dtm", precision: 6, null: false, comment: "Updated date and time"
    t.string "update_user_cd", limit: 6, null: false, comment: "Updated user"
  end

  create_table "user_detail", id: :decimal, force: :cascade do |t|
    t.string "user_id", limit: 6, null: false
    t.string "fullname", limit: 60, null: false
    t.binary "avatar"
    t.date "dob"
    t.string "email", limit: 60
    t.string "phone", limit: 20
    t.string "gender", limit: 1, null: false
  end

  add_foreign_key "user_detail", "tb_sct_login_user_kanri", column: "user_id", primary_key: "login_id", name: "user_detail_fk1"
end
