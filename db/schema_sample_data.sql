-- User
  CREATE TABLE "ADMIN"."TB_SCT_LOGIN_USER_KANRI" 
   (	"LOGIN_ID" CHAR(6 BYTE) NOT NULL ENABLE, 
	"LOGIN_USER_MEI" VARCHAR2(60 BYTE), 
	"LOGIN_PASSWORD" VARCHAR2(20 BYTE), 
	"KENGEN_ID" CHAR(2 BYTE), 
	"SAKUJO_FLG" CHAR(1 BYTE), 
	"INSERT_DTM" TIMESTAMP (6) NOT NULL ENABLE, 
	"INSERT_USER_CD" VARCHAR2(6 BYTE) NOT NULL ENABLE, 
	"UPDATE_DTM" TIMESTAMP (6) NOT NULL ENABLE, 
	"UPDATE_USER_CD" VARCHAR2(6 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "SYS_C007510" PRIMARY KEY ("LOGIN_ID")
  USING INDEX (CREATE UNIQUE INDEX "ADMIN"."TB_SCT_LOGIN_USER_KANRI_PK" ON "ADMIN"."TB_SCT_LOGIN_USER_KANRI" ("LOGIN_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" )  ENABLE, 
	 CONSTRAINT "SYS_C007506" CHECK ("INSERT_DTM" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007507" CHECK ("INSERT_USER_CD" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007508" CHECK ("UPDATE_DTM" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007509" CHECK ("UPDATE_USER_CD" IS NOT NULL) ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."LOGIN_ID" IS 'Login ID';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."LOGIN_USER_MEI" IS 'Login username';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."LOGIN_PASSWORD" IS 'Login password';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."KENGEN_ID" IS 'Authority ID';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."SAKUJO_FLG" IS 'Delete flag';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."INSERT_DTM" IS 'Registered Date';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."INSERT_USER_CD" IS 'Registered user';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."UPDATE_DTM" IS 'Update date and time';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."UPDATE_USER_CD" IS 'Update user';
   
--------------------------------------------------------
--  DDL for Sequence USER_DETAIL_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ADMIN"."USER_DETAIL_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Table USER_DETAIL
--------------------------------------------------------

  CREATE TABLE "ADMIN"."USER_DETAIL" 
   (	"ID" NUMBER, 
	"USER_ID" CHAR(6 BYTE), 
	"FULLNAME" VARCHAR2(60 BYTE), 
	"AVATAR" BLOB, 
	"DOB" DATE, 
	"EMAIL" VARCHAR2(60 BYTE), 
	"PHONE" VARCHAR2(20 BYTE), 
	"GENDER" CHAR(1 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" 
 LOB ("AVATAR") STORE AS SECUREFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING  NOCOMPRESS  KEEP_DUPLICATES ) ;
--------------------------------------------------------
--  DDL for Table TB_SCT_LOGIN_USER_KANRI
--------------------------------------------------------

  CREATE TABLE "ADMIN"."TB_SCT_LOGIN_USER_KANRI" 
   (	"LOGIN_ID" CHAR(6 BYTE), 
	"LOGIN_USER_MEI" VARCHAR2(60 BYTE), 
	"LOGIN_PASSWORD" VARCHAR2(20 BYTE), 
	"KENGEN_ID" CHAR(2 BYTE), 
	"SAKUJO_FLG" CHAR(1 BYTE), 
	"INSERT_DTM" TIMESTAMP (6), 
	"INSERT_USER_CD" VARCHAR2(6 BYTE), 
	"UPDATE_DTM" TIMESTAMP (6), 
	"UPDATE_USER_CD" VARCHAR2(6 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."LOGIN_ID" IS 'Login ID';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."LOGIN_USER_MEI" IS 'Login username';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."LOGIN_PASSWORD" IS 'Login password';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."KENGEN_ID" IS 'Authority ID';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."SAKUJO_FLG" IS 'Delete flag';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."INSERT_DTM" IS 'Registered Date';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."INSERT_USER_CD" IS 'Registered user';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."UPDATE_DTM" IS 'Update date and time';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_LOGIN_USER_KANRI"."UPDATE_USER_CD" IS 'Update user';
--------------------------------------------------------
--  DDL for Table USER_DETAIL
--------------------------------------------------------

  CREATE TABLE "ADMIN"."USER_DETAIL" 
   (	"ID" NUMBER, 
	"USER_ID" CHAR(6 BYTE), 
	"FULLNAME" VARCHAR2(60 BYTE), 
	"AVATAR" BLOB, 
	"DOB" DATE, 
	"EMAIL" VARCHAR2(60 BYTE), 
	"PHONE" VARCHAR2(20 BYTE), 
	"GENDER" CHAR(1 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" 
 LOB ("AVATAR") STORE AS SECUREFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING  NOCOMPRESS  KEEP_DUPLICATES ) ;
REM INSERTING into ADMIN.USER_DETAIL
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index USER_DETAIL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN"."USER_DETAIL_PK" ON "ADMIN"."USER_DETAIL" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Trigger USER_DETAIL_TRG
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE TRIGGER "ADMIN"."USER_DETAIL_TRG" 
BEFORE INSERT ON USER_DETAIL 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT USER_DETAIL_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ADMIN"."USER_DETAIL_TRG" ENABLE;
--------------------------------------------------------
--  Constraints for Table USER_DETAIL
--------------------------------------------------------

  ALTER TABLE "ADMIN"."USER_DETAIL" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."USER_DETAIL" MODIFY ("FULLNAME" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."USER_DETAIL" MODIFY ("GENDER" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."USER_DETAIL" ADD CONSTRAINT "USER_DETAIL_PK" PRIMARY KEY ("ID")
  USING INDEX "ADMIN"."USER_DETAIL_PK"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table USER_DETAIL
--------------------------------------------------------

  ALTER TABLE "ADMIN"."USER_DETAIL" ADD CONSTRAINT "USER_DETAIL_FK1" FOREIGN KEY ("USER_ID")
	  REFERENCES "ADMIN"."TB_SCT_LOGIN_USER_KANRI" ("LOGIN_ID") ENABLE;

-- Notification
  CREATE TABLE "ADMIN"."TB_SCT_OSHIRASE_KANRI" 
   (	"SEQ_NO" CHAR(4 BYTE) NOT NULL ENABLE, 
	"KEISAI_KUBUN" NUMBER, 
	"KEISAI_START_DTM" TIMESTAMP (6) NOT NULL ENABLE, 
	"KEISAI_END_DTM" TIMESTAMP (6) NOT NULL ENABLE, 
	"KEISAI_MSG" VARCHAR2(1024 BYTE) NOT NULL ENABLE, 
	"SAKUJO_FLG" CHAR(1 BYTE), 
	"INSERT_DTM" TIMESTAMP (6) NOT NULL ENABLE, 
	"INSERT_USER_CD" VARCHAR2(6 BYTE) NOT NULL ENABLE, 
	"UPDATE_DTM" TIMESTAMP (6) NOT NULL ENABLE, 
	"UPDATE_USER_CD" VARCHAR2(6 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "SYS_C007466" PRIMARY KEY ("SEQ_NO")
  USING INDEX (CREATE UNIQUE INDEX "ADMIN"."TB_SCT_OSHIRASE_KANRI_PK" ON "ADMIN"."TB_SCT_OSHIRASE_KANRI" ("SEQ_NO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" )  ENABLE, 
	 CONSTRAINT "SYS_C007459" CHECK ("KEISAI_START_DTM" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007460" CHECK ("KEISAI_END_DTM" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007461" CHECK ("KEISAI_MSG" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007462" CHECK ("INSERT_DTM" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007463" CHECK ("INSERT_USER_CD" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007464" CHECK ("UPDATE_DTM" IS NOT NULL) ENABLE, 
	 CONSTRAINT "SYS_C007465" CHECK ("UPDATE_USER_CD" IS NOT NULL) ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."SEQ_NO" IS 'Sequence number';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."KEISAI_KUBUN" IS 'Publication category';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."KEISAI_START_DTM" IS 'Publication start date and time';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."KEISAI_END_DTM" IS 'Publication end date and time';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."KEISAI_MSG" IS 'Publication message';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."SAKUJO_FLG" IS 'Delete flag';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."INSERT_DTM" IS 'Registered date and time';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."INSERT_USER_CD" IS 'Registered user';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."UPDATE_DTM" IS 'Updated date and time';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_OSHIRASE_KANRI"."UPDATE_USER_CD" IS 'Updated user';

-- Insert data notification
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0001', '1', TO_DATE('2021/09/21 10:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/09/28 00:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0002', '1', TO_DATE('2021/09/22 11:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/09/29 01:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 08:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 08:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0003', '1', TO_DATE('2021/09/23 12:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/09/30 02:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 09:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 09:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0004', '1', TO_DATE('2021/09/24 13:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/01 02:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 10:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 10:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0005', '1', TO_DATE('2021/09/25 14:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/02 03:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 11:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 11:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0006', '1', TO_DATE('2021/09/26 15:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/03 04:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 12:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 12:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0007', '1', TO_DATE('2021/09/27 16:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/04 05:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1', TO_DATE('2021/09/28 13:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 13:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0008', '1', TO_DATE('2021/09/28 17:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/05 06:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1', TO_DATE('2021/09/28 14:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 14:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0009', '2', TO_DATE('2021/09/28 18:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/05 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 15:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 15:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0010', '2', TO_DATE('2021/09/28 19:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/05 08:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 16:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 16:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0011', '1', TO_DATE('2021/09/29 20:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/06 09:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 17:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 17:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0012', '1', TO_DATE('2021/09/30 21:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/07 10:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 18:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 18:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0013', '1', TO_DATE('2021/10/01 22:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/08 11:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 19:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 19:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0014', '1', TO_DATE('2021/10/03 23:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/09 12:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 20:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 20:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_OSHIRASE_KANRI" (SEQ_NO, KEISAI_KUBUN, KEISAI_START_DTM, KEISAI_END_DTM, KEISAI_MSG, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('0015', '1', TO_DATE('2021/10/05 00:00:00', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2021/10/10 13:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0', TO_DATE('2021/09/28 21:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 21:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');

-- Insert data user
INSERT INTO "ADMIN"."TB_SCT_LOGIN_USER_KANRI"  (LOGIN_ID, LOGIN_USER_MEI, LOGIN_PASSWORD, KENGEN_ID, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('ad0001', 'admin', 'admin', '01', '0', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_LOGIN_USER_KANRI"  (LOGIN_ID, LOGIN_USER_MEI, LOGIN_PASSWORD, KENGEN_ID, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('us0001', 'user1', '1', '02', '0', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_LOGIN_USER_KANRI"  (LOGIN_ID, LOGIN_USER_MEI, LOGIN_PASSWORD, KENGEN_ID, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('us0002', 'user2', '2', '02', '0', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_LOGIN_USER_KANRI"  (LOGIN_ID, LOGIN_USER_MEI, LOGIN_PASSWORD, KENGEN_ID, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('us0003', 'user3', '3', '02', '0', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');
INSERT INTO "ADMIN"."TB_SCT_LOGIN_USER_KANRI"  (LOGIN_ID, LOGIN_USER_MEI, LOGIN_PASSWORD, KENGEN_ID, SAKUJO_FLG, INSERT_DTM, INSERT_USER_CD, UPDATE_DTM, UPDATE_USER_CD) VALUES ('us0004', 'user4', '4', '02', '0', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001', TO_DATE('2021/09/28 07:00:00', 'yyyy/mm/dd hh24:mi:ss'), 'ad0001');

CREATE TABLE ADMIN.TB_SCT_BB_JUCHU_MEISAI 
(
  SERIAL_NO VARCHAR2(4) NOT NULL 
, KAKIN_TEL_NO VARCHAR2(13) NOT NULL 
, TYAKUSIN_NUM NUMBER(3,0) NOT NULL 
, KAISEN_MEIGI VARCHAR2(72) NOT NULL 
, KAISEN_MEIGI_KANA VARCHAR2(72) NOT NULL 
, CONSTRAINT TB_SCT_BB_JUCHU_MEISAI_PK PRIMARY KEY 
  (
    SERIAL_NO 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX ADMIN.TB_SCT_BB_JUCHU_MEISAI_PK ON ADMIN.TB_SCT_BB_JUCHU_MEISAI (SERIAL_NO ASC) 
      TABLESPACE USERS 
      PCTFREE 10 
      INITRANS 2 
  )
  ENABLE 
) 
LOGGING 
TABLESPACE USERS 
PCTFREE 10 
PCTUSED 40 
INITRANS 1 
NOCOMPRESS;

CREATE TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" 
   (	"SERIAL_NO" VARCHAR2(4 BYTE), 
	"KAKIN_TEL_NO" VARCHAR2(13 BYTE), 
	"TYAKUSIN_NUM" NUMBER(3,0), 
	"KAISEN_MEIGI" VARCHAR2(72 BYTE), 
	"KAISEN_MEIGI_KANA" VARCHAR2(72 BYTE), 
	"DAIHYO_TEL_NO_KUBUN" CHAR(1 BYTE), 
	"NOB_KUBUN" CHAR(1 BYTE), 
	"WRITE_104_SECTION" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "ADMIN"."TB_SCT_BB_JUCHU_MEISAI"."KAKIN_TEL_NO" IS 'Billing phone number';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_BB_JUCHU_MEISAI"."DAIHYO_TEL_NO_KUBUN" IS 'Representative phone number section';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_BB_JUCHU_MEISAI"."NOB_KUBUN" IS 'NumberOnBB';
   COMMENT ON COLUMN "ADMIN"."TB_SCT_BB_JUCHU_MEISAI"."WRITE_104_SECTION" IS 'Write 104 section';
--------------------------------------------------------
--  DDL for Index TB_SCT_BB_JUCHU_MEISAI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN"."TB_SCT_BB_JUCHU_MEISAI_PK" ON "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" ("SERIAL_NO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table TB_SCT_BB_JUCHU_MEISAI
--------------------------------------------------------

  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("KAKIN_TEL_NO" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("TYAKUSIN_NUM" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("KAISEN_MEIGI" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("KAISEN_MEIGI_KANA" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" ADD CONSTRAINT "TB_SCT_BB_JUCHU_MEISAI_PK" PRIMARY KEY ("SERIAL_NO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("DAIHYO_TEL_NO_KUBUN" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("NOB_KUBUN" NOT NULL ENABLE);
  ALTER TABLE "ADMIN"."TB_SCT_BB_JUCHU_MEISAI" MODIFY ("WRITE_104_SECTION" NOT NULL ENABLE);