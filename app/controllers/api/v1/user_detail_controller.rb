class Api::V1::UserDetailController < ApplicationController
  before_action :authorized
  before_action :load_user_detail, only: %i(show)
  def create_user_detail(user_id)
    @user_detail = UserDetail.new(user_detail_params)
    @user_detail.user_id = user_id
    if @user_detail.save
      render json: {
        status: 201, 
        data: "User created"
      }, 
      status: :ok
      end
  end

  def show
    render json: {
      status: 200,
      data: @user_detail
    }, 
    status: :ok
  end

  def update_user_detail(user_id)
    @user = UserDetail.find_by user_id: user_id
    @user.update(user_detail_params)
    render json: {
      status: 200,
      data: @user
    }, 
    status: :ok
  end

  def create_or_update_user_detail
    user_id = decoded_token[0]["user_id"]
    @user = UserDetail.find_by user_id: user_id
    if @user
      update_user_detail(user_id)
    else
      create_user_detail(user_id)
    end
  end

  private
    def user_detail_params
        params.require(:user_detail).permit :user_id, :fullname, :avatar, :dob, :email, :phone, :gender
    end

  def load_user_detail
    @user_detail = UserDetail.find_by user_id: params[:id]
    return if @user_detail
    render json: {
      status: 404,
      message: "Not found"
    }, 
    status: :not_found
  end
end
