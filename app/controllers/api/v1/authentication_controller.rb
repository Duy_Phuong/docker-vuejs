class  Api::V1::AuthenticationController < ApplicationController
  def login
    @user = User.find_by(login_user_mei: user_params_login[:username])
    if @user.login_password == user_params_login[:password]
      token = encode_token({user_id: @user.login_id})
      render json: {id:@user.login_id,token: token}
    else
      render json: {errors: ["Invalid Username/Password"]}, status: :unauthorized
    end
  end

  private
    def user_params_login
      params.permit :username, :password
    end

  def payload(user)
    return nil unless user and user.id
    {
      auth_token: JsonWebToken.encode({user_id: user.id}),
      user: {id: user.id}
    }
  end
end
