class Api::V1::NotificationsController < ApplicationController
  before_action :authorized
  before_action :load_notification, only: %i(show)
  def index      
    @notification = Notification.where(
      "keisai_start_dtm <= ? 
      AND keisai_end_dtm >= ? 
      AND sakujo_flg = ?",Time.now,Time.now,"0")
    render json: {
      status: 200,
      data: @notification
    },
    status: :ok 
  end

  def show
    render json: {
      status: 200,
      data: @notifications
    }, 
    status: :ok
  end
  
  private
    def load_notification
      @notifications = Notification.find_by seq_no: params[:id]
      return if @notifications
      render json: {
        status: 404,
        message: "Not found"
      }, 
      status: :not_found
    end
end
