class Api::V1::PhoneNumberController < ApplicationController
  before_action :authorized
  
  def index
    @phone_number = PhoneNumber.all
    render json: {
      status: 200,
      data: @phone_number
    }, 
    status: :ok
  end

  def mockup_data
    @representative_phone_number_section =  [
      {"key": "1", "value": "Eurasian Collared-Dove"},
      {"key": "2", "value": "Bald Eagle"},
      {"key": "3", "value": "Cooper's Hawk"},
    ];
    @number_on_BB = [
      {"key": "1", "value": "Eurasian Collared-Dove"},
      {"key": "2", "value": "Bald Eagle"},
      {"key": "3", "value": "Cooper's Hawk"},
    ];
    @write_104_section = [
      {"key": "1", "value": "Eurasian Collared-Dove"},
      {"key": "2", "value": "Bald Eagle"},
      {"key": "3", "value": "Cooper's Hawk"},
    ];
    render json: {
      status: 200,
      representative_phone_number_section: @representative_phone_number_section,
      number_on_BB: @number_on_BB,
      write_104_section: @write_104_section
    },
    status: :ok 
  end

  def create
    @phone_number = PhoneNumber.new(phone_number_detail_params)
    if @phone_number.save
      render json: {
        status: 201, 
        data: "Phone number created"
      }, 
      status: :ok
    else
      render json: {
        status: 400,
        message: "Create phone number error"
      }
    end
  end

  def show
    @phone_number = PhoneNumber.find_by serial_no: params[:id]
    if @phone_number
      render json: {
        status: 200,
        data: @phone_number
      }, 
      status: :ok
    else
      render json: {
        status: 404,
        data: "phone number is not found"
      }
    end
  end

  def update
    @phone_number = PhoneNumber.find_by serial_no: params[:id]
    if @phone_number
      @phone_number.update(phone_number_detail_params)
      render json: {
        status: 200,
        data: @phone_number
      }, 
      status: :ok
    else
      render json: {
        status: 400,
        message: "Update phone number error"
      }
    end
  end

  private
    def phone_number_detail_params
      params.permit :serial_no, :kakin_tel_no, :tyakusin_num, :kaisen_meigi, :kaisen_meigi_kana, :daihyo_tel_no_kubun, :nob_kubun, :write_104_section, :name_in_the_phone_book, :name_in_the_phone_book_kana
    end
end
