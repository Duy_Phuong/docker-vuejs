class Api::V1::UsersController < ApplicationController
  before_action :authorized
  before_action :load_user, only: %i(show)
  def index
    @users = User.all
    render json: {
      status: 200,
      data: @users
    }, 
    status: :ok
  end

  def show
    render json: {
      status: 200,
      data: @users
    }, 
    status: :ok
  end

  private
    def load_user
      @users = User.find_by login_id: params[:id]
      return if @users
      render json: {
        status: 404,
        message: "Not found"
      }, 
      status: :not_found
    end
end
