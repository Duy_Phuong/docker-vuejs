class NotificationSerializer < ActiveModel::Serializer
  attributes :seq_no, 
  :keisai_kubun,
  :keisai_start_dtm, 
  :keisai_end_dtm, 
  :keisai_msg, 
  :sakujo_flg,
  :insert_dtm,
  :insert_user_cd,
  :update_dtm,
  :update_user_cd 
end
