class UserDetail < ApplicationRecord
    self.table_name = 'USER_DETAIL'
    self.sequence_name = 'USER_DETAIL_SEQ'
end