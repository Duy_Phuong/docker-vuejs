## Instructions for setting up the environment and running Ruby on rails, VueJS

##### Current version for the project:

- Ruby 3.0.1
- Rails 6.1
- Vue.js 2.6.14
- Vuetify  2.5
- Oracle database 19c windows

1. **Install ruby on rails**

   - To install Ruby on Windows you use Ruby Installer software. You can download Ruby Installer from [rubyinstaller.org](https://rubyinstaller.org/). After downloading you just need to double click on the installation file to start the installation process like a normal software.

   - On Windows, after installing Ruby using Ruby Installer, your computer will also be installed RubyGems.

   - You can install the Rails framework using the following command on the terminal (for Windows you use the command prompt):

     `$ gem install rails -v 6.1`

   - At the end of the installation you can check the Rails version on your machine using the following command:

     `$ rails -v`

2. **Install oracle database**

   - For detailed installation information, please refer to **docs/Guilde_install_oracle.md**

3. **Setup run ruby on rails environment.**

   - `cd .\VueRuby\rails_api\`

   - Update oracle changes to ruby on rails

     `rails db:migrate`

   - Run the server

     `rails server`

       ![alt text](./docs/images/run_ruby_on_rails.png "overview")

   - You can install [postman](https://www.postman.com/downloads/) to test the APIs. Details of the APIs you can refer to in the folder **docs/FORVAL_API.postman_collection**

4. **Install VueJS and Run VueJS environment**

   - `cd .\VueRuby\vuetify`

   - Project setup

     `npm install`

   - Run VueJS

     `npm run serve`

     ![alt text](./docs/images/run_vuejs1.png "overview")

     

     ![alt text](./docs/images/run_vuejs.png "overview")

   

